/* SPDX-FileCopyrightText: 2014-present Kriasoft */
/* SPDX-License-Identifier: MIT */

import { Box, Container, Typography, TypographyProps } from "@mui/material";
import { config } from "../../core/config.js";

export function Logo(props: TypographyProps): JSX.Element {
  const { sx, ...other } = props;

  return (
    <Container sx={{ display: "flex", gridGap: "1rem" }}>
      <Box component="img" sx={{ maxHeight: 48 }} src="../dist/pulley.webp" />
      <Typography
        sx={{
          ...sx,
          display: "flex",
          alignItems: "center",
          fontSize: "1.5rem",
          fontWeight: 500,
          fontFamily: "JetBrainsMono",
        }}
        variant="h1"
        {...other}
      >
        {config.app.name}
      </Typography>
    </Container>
  );
}
