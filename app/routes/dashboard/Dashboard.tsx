/* SPDX-FileCopyrightText: 2014-present Kriasoft */
/* SPDX-License-Identifier: MIT */

import { Api, GitHub } from "@mui/icons-material";
import {
  Box,
  Button,
  Card,
  CardContent,
  Container,
  Grid,
  Paper,
  Typography,
} from "@mui/material";
import { styled } from "@mui/material/styles";
import { usePageEffect } from "../../core/page.js";

// TODO(@brahste) Review this `styled` thingy here!
const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

export default function Home(): JSX.Element {
  usePageEffect({ title: "React App" });

  return (
    <Container
      sx={{ display: "flex", flexDirection: "column", py: "0vh" }}
      maxWidth="lg"
    >
      <Container sx={{ display: "flex", py: "20vh" }} maxWidth="lg">
        <Box
          sx={{
            display: "flex",
            flexDirection: "column",
            justifyContent: "center",
            gridGap: "1rem",
          }}
        >
          <Typography
            sx={{ mb: 2 }}
            variant="h1"
            align="left"
            fontFamily="JetBrainsMono"
          >
            Because people are invaluable, and tech is our tool.
          </Typography>

          <Typography sx={{ mb: 4 }} variant="h3" align="left">
            We&apos;re building a next generation content generator so your team
            can focus on human-critical tasks, not on machine repeatable ones.
          </Typography>
          <Box sx={{ display: "flex", gridGap: "1rem" }}>
            <Button
              variant="outlined"
              size="large"
              href={`/api`}
              children="Explorer API"
              startIcon={<Api />}
            />
            <Button
              variant="outlined"
              size="large"
              href="https://github.com/kriasoft/react-starter-kit"
              children="View on GitHub"
              startIcon={<GitHub />}
            />
          </Box>
        </Box>

        <Box
          component="img"
          sx={{ maxHeight: 512 }}
          src="../dist/macbook.png"
        />
      </Container>
      <Container
        component="div"
        sx={{
          display: "flex",
          // py: "20vh",
          gridGap: "1rem",
          flex: 1,
          justifyContent: "center",
        }}
      ></Container>
      <Grid container spacing={2}>
        <Grid item xs={4}>
          <Card
            elevation={0}
            sx={{
              // bgcolor: "transparent",
              // maxWidth: "25rem",
              minHeight: "10rem",
            }}
          >
            <CardContent
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography
                gutterBottom
                variant="h3"
                component="div"
                fontFamily="JetBrainsMono"
              >
                Streamline business development
              </Typography>
              <Typography variant="body2" color="text.secondary">
                by reducing the duration between bid consideration and decision.
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card
            elevation={0}
            sx={{
              // bgcolor: "transparent",
              // maxWidth: "25rem",
              minHeight: "10rem",
            }}
          >
            <CardContent
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography gutterBottom variant="h3" component="div">
                Focus your team
              </Typography>
              <Typography variant="body2" color="text.secondary">
                by reducing time spent on straightforward tasks such as creating
                boilerplate documentation.
              </Typography>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={4}>
          <Card
            elevation={0}
            sx={{
              // bgcolor: "transparent",
              // maxWidth: "25rem",
              minHeight: "10rem",
            }}
          >
            <CardContent
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "center",
              }}
            >
              <Typography gutterBottom variant="h3" component="div">
                Support data-driven decisions
              </Typography>
              <Typography variant="body2" color="text.secondary">
                by providing metrics about capability overlap, success rates on
                similar proposals, and estimated resource-to-revenue ratios.
              </Typography>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
    </Container>
  );
}
