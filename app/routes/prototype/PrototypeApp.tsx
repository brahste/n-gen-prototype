import {
  Box,
  Card,
  CardContent,
  Container,
  Grid,
  TextField,
  Typography,
} from "@mui/material";
import {
  Menu,
  MenuItem,
  ProSidebarProvider,
  Sidebar,
  SubMenu,
} from "react-pro-sidebar";

function PrototypeApplet(): JSX.Element {
  return (
    <Container sx={{ display: "flex", direction: "column" }}>
      <Grid container spacing={2} direction={"column"} paddingX={"1rem"}>
        <Grid item xs={4}>
          <Card
            variant="outlined"
            sx={{
              bgcolor: "lightgray",
              // maxWidth: "25rem",
              minHeight: "lg",
            }}
          >
            <CardContent
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <Typography gutterBottom fontFamily={"JetBrainsMono"}>
                Structure
              </Typography>
              <TextField multiline sx={{ bgcolor: "white" }}></TextField>
            </CardContent>
          </Card>
        </Grid>

        <Grid item xs={4}>
          <Card
            sx={{
              bgcolor: "lightgray",
              // maxWidth: "25rem",
              minHeight: "lg",
            }}
          >
            <CardContent
              sx={{
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <Typography gutterBottom fontFamily={"JetBrainsMono"}>
                Requirements Breakdown
              </Typography>
              <TextField multiline sx={{ bgcolor: "white" }}></TextField>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <Box
        component="div"
        sx={{ display: "flex", border: 1, borderColor: "red" }}
      >
        <Card
          variant="outlined"
          sx={{
            bgcolor: "lightgray",
            // maxWidth: "25rem",
            // minHeight: "lg",
          }}
        >
          <CardContent
            sx={{
              display: "flex",
              flexDirection: "column",
              alignItems: "flex-start",
            }}
          >
            <Typography gutterBottom fontFamily={"JetBrainsMono"}>
              Output Details
            </Typography>
          </CardContent>
        </Card>
      </Box>
    </Container>
  );
}

const PSideBar = (): JSX.Element => {
  return (
    <div>
      <Sidebar>
        <Menu>
          <SubMenu label="Charts">
            <MenuItem> Pie charts </MenuItem>
            <MenuItem> Line charts </MenuItem>
          </SubMenu>
          <MenuItem> Documentation </MenuItem>
          <MenuItem> Calendar </MenuItem>
        </Menu>
      </Sidebar>
    </div>
  );
};

export default function PrototypeApp(): JSX.Element {
  return (
    <ProSidebarProvider>
      <PrototypeApplet />
    </ProSidebarProvider>
  );
}
